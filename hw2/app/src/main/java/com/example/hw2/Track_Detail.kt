package com.example.hw2

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.view.menu.MenuView
import android.view.View
import android.widget.Toast
import com.example.hw2.ViewModel.TrackViewModel
import com.example.hw2.db.TracksDatabaseHelper
import com.example.hw2.model.Track
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_track__detail.*

class Track_Detail : AppCompatActivity() {



    private lateinit var track:Track
    private lateinit var viewModel: TrackViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track__detail)



        track = intent.extras!!.getSerializable("TRACK") as Track

        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)
        val image = track.get_image()
        val name = track.get_name()
        val artist = track.get_artist()

        nameView.setText(name)
        artistView.setText(artist)

        if(image == null){


        }
        else
        {
            Picasso.with(this).load(image).into(product_img)
        }

        Play.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {

                val db = TracksDatabaseHelper(view.context!!)
                db.addTrack(name, artist)

                val text = "PlayList Added!"
                val duration = Toast.LENGTH_SHORT

                val toast = Toast.makeText(view.context, text, duration)
                toast.show()


            }
        })

    }


    override fun onBackPressed() {
        this.finish()
    }
}
