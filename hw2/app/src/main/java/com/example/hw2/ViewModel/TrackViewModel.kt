package com.example.hw2.ViewModel

import android.annotation.SuppressLint
import android.app.Application
    import android.arch.lifecycle.AndroidViewModel

import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import com.example.hw2.model.Track
import com.example.hw2.util.QueryUtils


class TrackViewModel(application: Application): AndroidViewModel(application){
    private var _trackList :MutableLiveData<ArrayList<Track>> = MutableLiveData()

    fun getTrack(): MutableLiveData<ArrayList<Track>> {
        loadProducts("?method=chart.gettoptracks&api_key=42bd1b2a0b9b80943e641b39c1b1c1c6&format=json&limit=20")
        return _trackList
    }

    fun get_new_Track(query : String):MutableLiveData<ArrayList<Track>> {

            loadSearchProducts("?method=artist.gettoptracks&artist=$query&api_key=42bd1b2a0b9b80943e641b39c1b1c1c6&format=json")
            return _trackList

    }

    private fun loadProducts(query: String) {
        ProductAsyncTask().execute(query)
    }

    private fun loadSearchProducts(query: String) {
        ProductSearchAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            return QueryUtils.fetchTrackData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Track>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                Log.e("RESULTS", result.toString())
                _trackList.value = result
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class ProductSearchAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            return QueryUtils.fetchSerachTrackData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Track>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                Log.e("RESULTS", result.toString())
                _trackList.value = result
            }
        }
    }









}