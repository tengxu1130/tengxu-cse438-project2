package com.example.hw2.Fragment

import android.annotation.SuppressLint
import android.content.Context
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.example.hw2.R
import com.example.hw2.Track_Detail
import com.example.hw2.ViewModel.TrackViewModel
import com.example.hw2.model.Track
import kotlinx.android.synthetic.main.fragment_track_page.*
import kotlinx.android.synthetic.main.track_item.view.*


@SuppressLint("ValidFragment")
class Search_page (context: Context,query: String) : Fragment() {
    private var adapter = NewTrackAdapter()
    private var parentContext = context
    private var initialized: Boolean = false

    private var queryString: String = query

    private lateinit var viewModel: TrackViewModel

    private var TrackList: ArrayList<Track> = ArrayList()

    private var search_text = String()

    var thiscontext = context

    private var listener: Fragment_Main.OnFragmentInteractionListener? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_track_page, container, false)
    }

    override fun onStart()
    {
        super.onStart()

        val displayText = "Search for: $queryString"
        (activity as AppCompatActivity).supportActionBar?.title = displayText

        result_items_list.layoutManager = GridLayoutManager(parentContext,2)
//        result_items_list.layoutManager = LinearLayoutManager(parentContext)
        result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        val observer = Observer<ArrayList<Track>> {
            result_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return TrackList[p0].get_name() == TrackList[p1].get_name()
                }

                override fun getOldListSize(): Int {
                    return TrackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return TrackList[p0] == TrackList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            TrackList = it ?: ArrayList()
        }

//        search_edit_text.setOnEditorActionListener { _, actionId, _ ->
//            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                val searchText = search_edit_text.text
//                search_text = ""
//                search_edit_text.setText("")
//                if (searchText.toString() == "") {
//                    return@setOnEditorActionListener true
//                }
//                else {
//                    search_text = searchText.toString()
//                    return@setOnEditorActionListener false
//                }
//            }
//
//            return@setOnEditorActionListener false
//        }



//        if(search_text==null) {
        viewModel.get_new_Track(queryString).observe(this, observer)
//        }
//        else{
//            viewModel.get_new_Track(search_text).observe(this, observer)
//        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        thiscontext= context

    }

    inner class NewTrackAdapter: RecyclerView.Adapter<NewTrackAdapter.NewTrackViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NewTrackViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.track_item, p0, false)
            return NewTrackViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: NewTrackViewHolder, p1: Int) {
            val track = TrackList[p1]
            val productImages = track.get_image()
            var myintent = Intent()
            if (productImages == null) {
                // Do nothing for now
            }
            else {
                Picasso.with(this@Search_page.context).load(productImages).into(p0.productImg)
            }
            p0.productTitle.text = track.get_name()

            p0.row.setOnClickListener {
                myintent = Intent(thiscontext, Track_Detail::class.java)
                myintent.putExtra("TRACK", track)
                if(myintent != null){
                    startActivity(myintent)
                }
            }
        }

        override fun getItemCount(): Int {
            return TrackList.size
        }

        inner class NewTrackViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView
            var productImg: ImageView = itemView.product_img
            var productTitle: TextView = itemView.product_title
        }
    }








}
