package com.example.hw2.Fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.hw2.R
import com.example.hw2.ViewModel.PlayViewModel
import com.example.hw2.model.Play
import kotlinx.android.synthetic.main.fragment_db_page.*
import kotlinx.android.synthetic.main.play_list_item.view.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DB_page.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DB_page.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DB_page : Fragment() {

    private var adapter = PlayAdapter()
    private lateinit var viewModel: PlayViewModel

    private var PlayList: ArrayList<Play> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_db_page, container, false)
    }


    override fun onStart() {
        super.onStart()

        result_items_list.layoutManager = LinearLayoutManager(this.context)
        result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(PlayViewModel::class.java)

        val observer = Observer<ArrayList<Play>> {
            result_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if(p0 >= PlayList.size || p1 >= PlayList.size) {
                        return false;
                    }
                    return PlayList[p0].name == PlayList[p1].name
                }

                override fun getOldListSize(): Int {
                    return PlayList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return PlayList[p0] == PlayList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            PlayList = it ?: ArrayList()
        }


        viewModel.getTracks().observe(this, observer)
    }



    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }

    }

    inner class PlayAdapter: RecyclerView.Adapter<PlayAdapter.PlayViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PlayViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.play_list_item, p0, false)
            return PlayViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: PlayViewHolder, p1: Int) {
            val play = PlayList[p1]
            p0.name.text = play.name
            p0.artist.text = play.artist
        }

        override fun getItemCount(): Int {
            return PlayList.size
        }

        inner class PlayViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var name: TextView = itemView.name
            var artist: TextView = itemView.artist
        }
    }


}
