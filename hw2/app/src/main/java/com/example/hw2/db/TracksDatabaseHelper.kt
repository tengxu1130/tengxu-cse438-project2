package com.example.hw2.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class TracksDatabaseHelper(context: Context): SQLiteOpenHelper(context, dbsetting.DB_NAME, null, dbsetting.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + dbsetting.DBTrackEntry.TABLE + " ( " +
                dbsetting.DBTrackEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                dbsetting.DBTrackEntry.COL_NAME + " TEXT NULL, " +
                dbsetting.DBTrackEntry.COL_ARTIST + " TEXT NULL)"



        db?.execSQL(createFavoritesTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + dbsetting.DBTrackEntry.TABLE)
        onCreate(db)
    }

    fun addTrack(name: String, artist: String) {
        // Gets the data repository in write mode
        val db = this.writableDatabase

// Create a new map of values, where column names are the keys
        val values = ContentValues().apply {
            put(dbsetting.DBTrackEntry.COL_NAME,name )
            put(dbsetting.DBTrackEntry.COL_ARTIST, artist)
        }

// Insert the new row, returning the primary key value of the new row
        val newRowId = db?.insert(dbsetting.DBTrackEntry.TABLE, null, values)
    }
}