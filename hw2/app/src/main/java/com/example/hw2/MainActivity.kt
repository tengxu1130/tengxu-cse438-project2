package com.example.hw2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.hw2.Fragment.DB_page
import com.example.hw2.Fragment.Search_page
import com.example.hw2.Fragment.Track_Home
import com.example.hw2.Fragment.Track_page
import com.example.hw2.db.TracksDatabaseHelper
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fm = supportFragmentManager

        val ft = fm.beginTransaction()
        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)

//        val db = TracksDatabaseHelper(MainActivity())
//        db.addTrack("abc","bcd")



        ft.commit()


//        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)

//
//        val fm = supportFragmentManager
//
//        val ft = fm.beginTransaction()



    }
    class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {

                   Track_Home(MainActivity())


                }
                else -> DB_page()
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Tracks"
                else -> {
                    return "Playlist"
                }
            }
        }
    }
}
