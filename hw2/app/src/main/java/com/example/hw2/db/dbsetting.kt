package com.example.hw2.db

import android.provider.BaseColumns

class dbsetting{

    companion object {
        const val DB_NAME = "Track.db"
        const val DB_VERSION = 1
    }

    class DBTrackEntry: BaseColumns {
        companion object {
            const val TABLE = "Playlist"
            const val ID = BaseColumns._ID
            const val COL_NAME = "name"
            const val COL_ARTIST = "artist"
        }
    }


}