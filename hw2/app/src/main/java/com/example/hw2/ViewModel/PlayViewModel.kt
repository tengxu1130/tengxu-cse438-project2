package com.example.hw2.ViewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.provider.BaseColumns
import com.example.hw2.db.TracksDatabaseHelper
import com.example.hw2.db.dbsetting
import com.example.hw2.model.Play
import com.example.hw2.model.Track

class PlayViewModel(application: Application): AndroidViewModel(application) {
    private var _TrackList: MutableLiveData<ArrayList<Play>> = MutableLiveData()
    private var _TrackDBHelper: TracksDatabaseHelper = TracksDatabaseHelper(application)


    fun getTracks(): MutableLiveData<ArrayList<Play>> {
        loadTrack()
        return _TrackList
    }

    private fun loadTrack() {
        val db = _TrackDBHelper.readableDatabase

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        val projection = arrayOf(BaseColumns._ID, dbsetting.DBTrackEntry.COL_NAME, dbsetting.DBTrackEntry.COL_ARTIST)

        val cursor = db.query(
            dbsetting.DBTrackEntry.TABLE,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )

        var Tracks = ArrayList<Play>()
        with(cursor) {
            with(cursor) {
                while (moveToNext()) {
                    val name = getString(getColumnIndexOrThrow(dbsetting.DBTrackEntry.COL_NAME))
                    val artist = getString(getColumnIndexOrThrow(dbsetting.DBTrackEntry.COL_ARTIST))
                    val j = Play(name, artist)
                    Tracks.add(j)
                }
            }
            _TrackList.value = Tracks
        }

    }
}