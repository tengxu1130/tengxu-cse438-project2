package com.example.hw2.util

import android.text.TextUtils
import android.util.Log
import com.example.hw2.model.Track
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtils {
    companion object{
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/"

        fun fetchTrackData(jsonQueryString: String): ArrayList<Track>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJson(jsonResponse)
        }


        fun fetchSerachTrackData(jsonQueryString: String): ArrayList<Track>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractSearchDataFromJson(jsonResponse)
        }

        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            }
            catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }
        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                }
                else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            }
            finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }


        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }


        private fun extractDataFromJson(productJson: String?): ArrayList<Track>?{
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val TrackList =ArrayList<Track>()

            try{

                val tracksbaseJsonResponse = JSONObject(productJson)
                var trackbaseJsonResponse = returnValueOrDefault<JSONObject>(tracksbaseJsonResponse,"tracks") as JSONObject
                val baseJsonResponse = returnValueOrDefault<JSONArray>(trackbaseJsonResponse,"track") as JSONArray
                for (i in 0 until baseJsonResponse.length()){

                    val TrackObject = baseJsonResponse.getJSONObject(i)

                    val images = returnValueOrDefault<JSONArray>(TrackObject, "image") as JSONArray?

                    var image_list = String()

                    var artist_name = String()

                    if(images != null)
                    {
                        val image_url = images.getJSONObject(3)

                        val image_val = returnValueOrDefault<String>(image_url, "#text")

                         image_list = image_val.toString()
                    }

                    val artist = returnValueOrDefault<JSONObject>(TrackObject, "artist") as JSONObject?

                    if(artist != null)
                    {

                        artist_name = returnValueOrDefault<String>(artist,"name") as String

                    }

                    TrackList.add(Track(
                        returnValueOrDefault<String>(TrackObject,"name") as String,
                        artist_name,
                        image_list
                        )
                    )
                }



            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the category JSON results", e)
            }

            return TrackList

        }


        private fun extractSearchDataFromJson(productJson: String?): ArrayList<Track>?{
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val TrackList =ArrayList<Track>()

            try{

                val tracksbaseJsonResponse = JSONObject(productJson)

                val   trackbaseJsonResponse = returnValueOrDefault<JSONObject>(tracksbaseJsonResponse,"toptracks") as JSONObject

                if(trackbaseJsonResponse==null)
                {
                    return null
                }

                val baseJsonResponse = returnValueOrDefault<JSONArray>(trackbaseJsonResponse,"track") as JSONArray
                for (i in 0 until baseJsonResponse.length()){

                    val TrackObject = baseJsonResponse.getJSONObject(i)

                    val images = returnValueOrDefault<JSONArray>(TrackObject, "image") as JSONArray?

                    var image_list = String()

                    var artist_name = String()

                    if(images != null)
                    {
                        val image_url = images.getJSONObject(3)

                        val image_val = returnValueOrDefault<String>(image_url, "#text")

                        image_list = image_val.toString()
                    }

                    val artist = returnValueOrDefault<JSONObject>(TrackObject, "artist") as JSONObject?

                    if(artist != null)
                    {

                        artist_name = returnValueOrDefault<String>(artist,"name") as String

                    }

                    TrackList.add(Track(
                        returnValueOrDefault<String>(TrackObject,"name") as String,
                        artist_name,
                        image_list
                    )
                    )
                }



            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the category JSON results", e)
            }

            return TrackList

        }


        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    }
                    else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    }
                    else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    }
                    else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    }
                    else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    }
                    else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }


    }
}